#!/usr/bin/python3
# File name: election.py
# This program finds the frequency of words in tweets. It requires inputs from the user to
# get the date and hour in which should be searched. The user is then asked to enter the words
# which frequency has to be found and the results will be printed. 
# Author: Martijn Baas
# Date: 19-03-2017

import gzip

def intro():
    """short introduction print statement"""
    print("This program finds the frequency of words in tweets using the twitter2 corpus.")
    print("This progrma should be executed from the directory /home/s1234567/ where the student number should be in the second part.\n")

def get_path():
    """gets input from the user and returns the path as a string"""
    year = input("Enter the year(2010-2017): ")
    months = input("Enter the month(01-12): ")
    day = input("Enter the day(01-31): ")
    hour = input("Enter the hour(00-23): ")
    filename = "{0}{1}{2}:{3}.out.gz".format(year, months, day, hour)
    return "/net/corpora/twitter2/Tweets/Tekst/{0}/{1}/{2}".format(year, months, filename)

def read_gz_file(path):
    """returns a list containing the lines in a .gz file"""
    with gzip.open(path, mode="rt") as f:
         l = [line.rstrip().split("\t", 1)[1] for line in f]
    return l

def tokenize(l):
    """returns a list containing lines with punctuation removed"""
    tl = []
    for line in l:
        newline = ""
        for c in line:
            if c.isalnum():
                newline += c
            else:
                newline += " "
        tl.append(newline)
    return tl

def get_scores(linelist):
    """gets input from user what parties should be searched for"""
    p = input("Enter the parties that should be searched for and seperate them by a space: ")
    pl = []
    for item in p.split():
        freq = 0
        for line in linelist:
            for word in line.split():
                if word.lower() == item.lower():
                    freq += 1
        pl.append((freq, item))
    return pl

def main():
    intro()
    try:
        path = get_path()
        tweets = tokenize(read_gz_file(path))
        elec_list = get_scores(tweets)
        print()
        for item in sorted(elec_list, reverse=True):
            print("{0:<10}{1}".format(item[0], item[1]))
   
    except FileNotFoundError:
        print("Usage: election.py, Date or hour was not given correctly")

if __name__ == "__main__":
    main()
