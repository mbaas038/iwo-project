#!/bin/bash
# 0. All echo commands are used to introduce the solutions
# 1. The file is being accessed with zless | With tweet2tab show only the text from the tweets | The amount of lines is being counted
# 2. The file is being accessed with zless | With tweet2tab show only the text from the tweets | Remove duplicate tweets with awk |
# The amount of lines is being counted
# 3. The file is being accessed with zless | With tweet2tab show only the text from the tweets | Remove duplicate tweets with awk |
# Search for tweets that start with 'RT @' to show only retweets | The amount of lines is being counted
# 4. The file is being accessed with zless | With tweet2tab show only the text from the tweets | Remove duplicate tweets with awk |
# Search for tweets that do not start with 'RT @' to show tweets that are not retweets | The amount of lines is being counted

#0.
echo 'The amount of tweets in the sample is:'

#1.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

#0.
echo 'The amount of unique tweets in the sample is:'

#2.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | wc -l

#0.
echo 'The amount of retweets in the sample (out of the unique tweets) is:'

#3.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep '^RT @' | wc -l

#0.
echo 'The first 20 tweets in the sample which are not retweets are:'

#4.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep -v 'RT @' | head -n20
